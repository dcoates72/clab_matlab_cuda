/*==========================================================
 * cuda_reset.c - Calls CUDA function to reset the GPU
 *    (clear memory, unload processes, etc.)
 *
 * Call like this: cuda_reset()
 *
 * This was needed since our Tesla K40m got too hot if 
 * anything stayed resident --  MATLAB/OpenCV/Jupyter kept 
 * a small kernel as a running process.
 *
 * After this call in MATLAB, need to re-acquire the GPU using
 * something like: gpuDevice(1);, or there will be errors.
 *
 * To build:
 * mex -I/usr/local/cuda-10.1/targets/x86_64-linux/include -L/usr/local/cuda-10.1/targets/x86_64-linux/lib -lcudart cuda_reset.c
 *
 *========================================================*/

#include "mex.h"

#include "cuda_runtime.h"
#include <stdio.h>

int cuda_reset(void){
    cudaDeviceReset();
    //printf("GPU Reset!\n");
    //cudaDeviceSynchronize();
    return 1;
}

/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // DRC: Ignore everything (parameters, etc.)

    cuda_reset();
}
