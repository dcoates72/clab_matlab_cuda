g=gpuDevice(1);

tic;
A4 = rand(9000,9000);
B4 = fft(A4);
time4 = toc;

tic;
A5 = gpuArray(rand(9000,9000));
%A5 = parallel.gpu.GPUArray.rand(3000,3000);
B5 = fft(A5);
%B5 = gather(B5);
time5 = toc;

speedUp = time4/time5;
disp(speedUp);

cuda_reset