/*==========================================================
 * cuda_get_temp.c - Calls CUDA function to get the temperature
 * (C) of the (first) GPU
 *
 * Call like this: cuda_get_temp()
 *
 * This could be helpful to monitor if our Tesla K40m is
 * getting too hot.
 *
 * To build:
 * mex -I/usr/local/cuda-10.1/targets/x86_64-linux/include -L/usr/local/cuda-10.1/targets/x86_64-linux/lib -lnvidia-ml cuda_get_temp.c
 *
 *========================================================*/

#include "mex.h"

#include <stdio.h>
#include <nvml.h>

unsigned int get_temp()
{
    nvmlReturn_t result;
    unsigned int temp;

    // First initialize NVML library
    result = nvmlInit();
    if (NVML_SUCCESS != result)
    {
        printf("Failed to initialize NVML: %s\n", nvmlErrorString(result));

        printf("Press ENTER to continue...\n");
        getchar();
        return 1;
    }

    nvmlDevice_t device;

    result = nvmlDeviceGetHandleByIndex(0, &device);
    if (NVML_SUCCESS != result)
    {
	printf("Failed to get handle for device %i: %s\n", 0, nvmlErrorString(result));
	goto Error;
    }

    result = nvmlDeviceGetTemperature(device, NVML_TEMPERATURE_GPU, &temp);
    if (NVML_SUCCESS != result) {
	printf("Failed to get temperature of device %i: %s\n", 0, nvmlErrorString(result));
    }
    printf("%d\n", temp);

    result = nvmlShutdown();
    if (NVML_SUCCESS != result)
        printf("Failed to shutdown NVML: %s\n", nvmlErrorString(result));

    return temp;

Error:
    result = nvmlShutdown();
    if (NVML_SUCCESS != result)
        printf("Failed to shutdown NVML: %s\n", nvmlErrorString(result));

    //printf("Press ENTER to continue...\n");
    //getchar();
    
    return 1000;
}

/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    unsigned int temp;
    double *y;

    // TODO: Error checking
    //} else if (nlhs > 1) {
        //mexErrMsgIdAndTxt("MATLAB:timestwo:maxlhs", "Too many output arguments.");
    //}

    /* Create matrix for the return argument. */

    plhs[0] = mxCreateDoubleMatrix((mwSize)1, (mwSize)1, mxREAL);
    y = mxGetPr(plhs[0]);

    temp=get_temp();

    y[0] = (double)temp;
}
